package com.example.shibe.model

import com.example.shibe.model.remote.DogService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class DogRepo @Inject constructor(private val dogService: DogService) {
//    private val dogService by lazy { DogService.getInstance() }

    suspend fun getDogs(count: Int) = withContext(Dispatchers.IO) {
        dogService.getDogs(count)
    }
}