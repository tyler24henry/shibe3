package com.example.shibe.view.dog

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.shibe.model.DogRepo
import com.example.shibe.viewmodel.DogViewModel
import javax.inject.Inject

class DogViewModelFactory